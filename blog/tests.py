from datetime import datetime
from django.test import TestCase
from django.utils import timezone
from blog.models import ArticleModel


class ArticleTest(TestCase):
    def test_article_created_success(self):
        ArticleModel.objects.create(title='Test Article',
                                    category='test',
                                    author='Test Author',
                                    content='Test Content',
                                    created_at=datetime.now(tz=timezone.utc))

        article = ArticleModel.objects.get(title='Test Article')

        self.assertEqual(article.category, 'test')


class BlogPagesTest(TestCase):
    def test_home_page_content(self):
        res = self.client.get('/blog/')

        self.assertEqual(res.content, b'Welcome to my blog!')
